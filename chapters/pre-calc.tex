\chapter{Pre-Calculus}

In this chapter, we will be reviewing functions and their friends, including odd/even function,
increasing/decreasing function, piecewise function and interval notation.

\section{Functions}{}{}

We have all learned and been using function for years. This concept gets increasingly
important when we start our journey in the world of Calculus. It is thereby worth
understanding -- What excatly is a function?

\begin{definition}\label{def:function}\index{functions!function}
A function is a relation between sets, where for each input, there
is exactly one output. (Fowler \& Snapp, 2014)
\end{definition}

Vertical line test is a visual way to quickly determine whether a graph
represents a function or not. By definition, A function can only have one output for
each unique input. If a vertical line intersects a curve more than once,
we know that at that x-position, there presents more than one outputs,
and so, the graph does not represent a function. Thus, we can use a vertical line
and move it from the leftmost to the rightmost of the function and see if it touches
the graph more than once.

\begin{example}
The plot~\ref{plot:example of a function} shows an
example of a quadratic function \(f(x)=\protect x^2 + 2x + 3\). Through the Vertical
line test, we can scan through the graph and confirm that no vertical line can touch
the curve more than once, thereby asserting that the graph indeed represents a function.
\end{example}

\begin{example}
The plot~\ref{plot:example of a non-function} shows an case where a vertical line (e.g. \(x=0\))
can touch the graph twice, making this a non-function (we can still call it a relation).
\end{example}
\begin{marginfigure}[-5in]
  \begin{adjustbox}{width=\marginparwidth}
  \begin{tikzpicture}
	\begin{axis}[
            domain=-5:5,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            ticks=none,
          ]
	  \addplot [very thick, penColor, smooth] {x^2 + 2*x + 3};
    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{A plot of \(f(x)=\protect x^2 + 2x + 3\).}\label{plot:example of a function}  
\end{marginfigure}

\begin{marginfigure}[-1in]
  \begin{adjustbox}{width=\marginparwidth}
  \begin{tikzpicture}
	\begin{axis}[
            xmin=-2, xmax=2, ymin=-2, ymax=2,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            ticks=none,
          ]
          \draw[color=penColor, very thick](0,0) circle (1);

    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{A plot of \(x^2+y^2=1\).}\label{plot:example of a non-function}  
\end{marginfigure}

\section{Interval Notation}{}{}
Interval notation is a very handy way to represent a range. We will be using this a lot 
in Calculus.
\begin{center}
  [a, b] – an range from a to b, including endpoints

  (a, b) – an range from a to b, excluding endpoints
\end{center}
We can also combine ``()`` and ``[]`` to only include one endpoint, as shown in the case below.
\begin{center}
  (a, b] – an range from a to b, including only the right endpoint.

  [a, b) – an range from a to b, including only the left endpoint.
\end{center}

\section{Piecewise Function}{}{}
\begin{definition}\label{def:piecewise_function}\index{functions!piecewise function}
  Piecewise Function is a function defined by two or more equations over a specific domain.
\end{definition}

Piecewise functions allow us to represent a complex relation.
For example, the absolute value function can be represented using the piecewise function,
\[\mathit{abs}(x) = \begin{cases} 
  x & x > 0 \\
  -x & x \leq 0
\end{cases}
\] 

\section{Even and Odd Functions}{}{}
\begin{definition}\label{def:even_odd_function}\index{functions!even and odd function}
Even functions are symmetric along the y-axis.

Odd functions are symmetric to the origin.
\end{definition}

If \(f(-x) = f(x)\), then f is an even function.

If \(f(-x) = -f(x)\), then f is an odd function.

\section{Increasing / Decreasing Functions}{}{}
\begin{definition}\label{def:inc_dec_func}\index{functions!increasing and decreasing function}
  A function is increasing on an interval if \(f(x_1) < f(x_2)\) whenever \(x_1 < x_2\) in I.

  A function is decreasing on an interval if \(f(x_1) > f(x_2)\) whenever \(x_1 < x_2\) in I.  
\end{definition}

\begin{example}
The plot~\ref{plot:inc_function} shows an example of an increasing function. Very straight-forwardly,
the function only increases and never decreases when we go from the left to right.
\end{example}
We will soon know how to determine the increasing/decreasing interval of a function without
the assistance of graphs.

\begin{marginfigure}[-1in]
  \begin{adjustbox}{width=\marginparwidth}
  \begin{tikzpicture}
	\begin{axis}[
            domain=-5:5,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            ticks=none,
          ]
	  \addplot [very thick, penColor, smooth] { x^3 };
    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{A plot of \(y=x^3\).}\label{plot:inc_function}  
\end{marginfigure}

\section{Combination of Functions}{}{}
Sometimes we want to combine two functions to produce a new function. For example, 
we might want to know the total price for two products, a and b, whose prices vary by time.
If the price function of the two product are \(a(t), b(t)\) respectively, the total cost
can be represented as \(\mathit{total}(t)=(a+b)(t)=a(t)+b(t)\).

Similarly, we can also substract, multiply and divide two functions.
\[
  \begin{aligned}
  (a+b)(x)&=a(x)+b(x)\\ 
  (a-b)(x)&=a(x)-b(x)\\ 
  (a*b)(x)&=a(x)b(x)\\ 
  (a/b)(x)&=\frac{a(x)}{b(x)}\\ 
  \end{aligned}
\]

Lastly, we can have composition function in which two functions are applied in succession.
That is, the output of a function is used as the input of another.
\[
  (f \circ g)(x)=f(g(x))
\]

\begin{example}
  If we have \(f(x)=x+1\) and \(g(x)=x^2\)\dots
  \[
    \begin{aligned}
  (a+b)(x)&=a(x)+b(x)=x+1+x^2\\ 
  (a-b)(x)&=a(x)-b(x)=x+1-x^2\\ 
  (a*b)(x)&=a(x)b(x)=(x+1)x^2=x^3+x^2\\ 
  (a/b)(x)&=\frac{a(x)}{b(x)}=\frac{x+1}{x^2}\\ 
  (f \circ g)(x)&=f(g(x))=x^2+1\\ 
  (g \circ f)(x)&=g(f(x))=(x+1)^2
\end{aligned}
  \]
\end{example}

\section{Inverse Functions}{}{}

If a function maps every input to exactly one output, an inverse of that function
maps every output back to its input. Simply put, an inverse function is the reverse
of a function.

The inverse of a function can be represented as \(f^{-1}(x)\). The following is generally
true for \(g(x)=f^{-1}(x)\),
\[g(f(x))=x\]

How do we find the inverse function then?
Algebraically, we can obtain the inverse of a function by interchanging x and y and then solving for y.
Graphically, the inverse function is the reflection of the original function along the line of \(y=x\).

\begin{example}
  Say we have a function \(y=x^3\) and we want to obtain its inverse function \(y^{-1}\).
  First we interchange x and y: \[x=y^3\]
  and then we can solve for y, \[y=\sqrt[3]{x}\]

  The plot~\ref{plot:example of inverse} shows that the inverse function can also be obtained by reflecting
  the original function along the line \(y=x\).

\end{example}

\begin{warning}
  Some might confuse the symbol for inverse function and the symbol used for reciprocal.
  While \(f^{-1}(x)\) means the inverse function of f, \({f(x)}^{-1}\) means \(\frac{1}{f(x)}\).
  For example, \(\sin^{-1}(x)\) means the inverse function of \(\sin(x)\), not the reciprocal of sin.
\end{warning}

\begin{marginfigure}[-5in]
  \begin{adjustbox}{width=\marginparwidth}
  \begin{tikzpicture}
    \begin{axis}[
      xmin=-5, xmax=5, ymin=-5, ymax=5,
      axis lines =middle, xlabel=$x$, ylabel=$y$,
      every axis y label/.style={at=(current axis.above origin),anchor=south},
      every axis x label/.style={at=(current axis.right of origin),anchor=west},
      grid=both,
      grid style={dashed, gridColor},
      ticks=none,
      ]
      \addplot [very thick, penColor, smooth] {x^3};
      \addplot [thick, penColor2, smooth, dashed] {x};
      \addplot [very thick, penColor3, smooth] {x/abs(x)*abs(x)^(1/3)};
      \node at (axis cs:0.5,3) [anchor=west] {\color{penColor}$f(x)$};
      \node at (axis cs:3.2,3) [anchor=west] {\color{penColor2}$y=x$};
      \node at (axis cs:3,0.8) [anchor=west] {\color{penColor3}$f^{-1}(x)$};
    \end{axis}
  \end{tikzpicture}
\end{adjustbox}  
\caption{A plot of \(f(x)=\protect x^3\) and its inverse.}\label{plot:example of inverse}  
\end{marginfigure}

\section{One-to-one Function}{}{}
\begin{definition}\label{def:one_to_one_func}\index{functions!one-to-one function}
  A function is \textbf{one-to-one} if it never takes on the same value twice; that is \(f(x_1) \neq f(x_2)\) whenever \(x_1 \neq x_2\).
\end{definition}

Like how we test if a relation is function, we also have a horizontal line test that helps us determine
whether a function is one-to-one function.

A function f has an inverse function if and only if no horizontal line intersects the graph of f at more
than one point, that is, when it is a one-to-one function.


\begin{exercises}

  \noindent Determine if each of the following is a function. If it is a function, write them in their explicit form.
  \twocol%
    \begin{exercise}  \(x^2+y^2=1\)
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise}  \(y^3-x^2=1\)
    \begin{answer} Yes. \(y=\sqrt[3]{1+x^2}\)
    \end{answer}\end{exercise}

    \begin{exercise}  \(y=1+x^2y\)
    \begin{answer} Yes. \(y=\frac{1}{1-x^2}\)
    \end{answer}\end{exercise}

    \begin{exercise}  \(y=1+x^2y^2\)
    \begin{answer} No.
    \end{answer}\end{exercise}
  \endtwocol%

  \noindent Determine if each of the following falls into the given interval.
  \twocol%
    \begin{exercise}  \([1,2]\),\(\frac{3}{2}\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise}  \([1,2)\),\(1\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise}  \([1,2)\),\(2\)
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise}  \((-\infty,5)\),\(8\)
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise}  \(\mathbb{R}\),\(1\)
    \begin{answer} Yes
    \end{answer}\end{exercise}

    \begin{exercise}  \((5,6)\cup[10,12]\),\(10\)
    \begin{answer} Yes
    \end{answer}\end{exercise}
  \endtwocol%

  \noindent Evaulate the following piecewise functions.
  \begin{exercise}  
    \(f(x)=\begin{cases} 
      2x & x > 0 \\
      -x & x \leq 0
    \end{cases}\)
    
    \(f(1)\)
  \begin{answer} \(2\)
  \end{answer}\end{exercise}

  \begin{exercise}  
    \(f(x)=\begin{cases} 
      x-1 & x^2 > 2 \\
      x+1 & x^2 \leq 2
    \end{cases}\)
    
    \(f(2)\)
  \begin{answer} \(1\)
  \end{answer}\end{exercise}

  \begin{exercise} 
    \(f(x)=\begin{cases} 
      f(x-1)+1 & x \mathit{\ is\ odd} \\
      2f(\frac{x}{2}) & x \mathit{\ is\ even} \\
      1 & x=0
    \end{cases}\)
    
    \(f(6)\)
  \begin{answer} \(f(6)=2f(3)=2(f(2)+1)=2(2f(1)+1)=2(2(f(0)+1)+1)=10\)
  \end{answer}\end{exercise}

  \noindent Determine if the following function odd, even or neither
  \begin{exercise} \(f(x)=x\)
  \begin{answer} \(f(-x)=-x=-f(x)\), odd.
  \end{answer}\end{exercise}

  \begin{exercise} \(f(x)=x^2\)
  \begin{answer} \(f(-x)=x^2=f(x)\), even.
  \end{answer}\end{exercise}

  \begin{exercise} \(f(x)=x^2+x\)
  \begin{answer} \(f(-x)=x^2-x\), neither.
  \end{answer}\end{exercise}

  \noindent Determine the increasing and decreasing interval of the following function.
  \begin{tikzpicture}
    \begin{axis}[
              domain=-5:5,
              ymax=5,ymin=-5,
              axis lines =middle, xlabel=$x$, ylabel=$y$,
              every axis y label/.style={at=(current axis.above origin),anchor=south},
              every axis x label/.style={at=(current axis.right of origin),anchor=west},
              grid=both,
              grid style={dashed, gridColor},
              xtick={-5,...,5},
              ytick={-5,...,5},
            ]
      \addplot [very thick, penColor, smooth] {x^3-3*x^2};
        \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(0,0)};  %% closed hole
        \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(2,-4)};  %% closed hole
      \end{axis}
  \end{tikzpicture}

  \begin{exercise} Increasing interval
  \begin{answer} \((-\infty,0)\cup(2,\infty)\)
  \end{answer}\end{exercise}

  \begin{exercise} Decreasing interval
  \begin{answer} \((0,2)\)
  \end{answer}\end{exercise}

  \noindent Evaulate the following expression. \(f(x)=x+1\), \(g(x)=x^2+x\)
  \twocol%
    \begin{exercise} \(f(x)+g(x)\)
    \begin{answer} \(f(x)+g(x)=x+1+x^2+x=x^2+2x+1\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)+g(x)\)
    \begin{answer} \(f(x)g(x)=(x+1)(x^2+x)=x^3+2x^2+x\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(g(x))\)
    \begin{answer} \(f(g(x))=(x^2+x)+1=x^2+x+1\)
    \end{answer}\end{exercise}

    \begin{exercise} \((g \circ f)(x)\)
    \begin{answer} \((g \circ f)=g(f(x))=(x+1)^2+(x+1)=x^2+3x+2\)
    \end{answer}\end{exercise}
  \endtwocol%

  \noindent Find the inverse of the following functions.
  \twocol%
    \begin{exercise} \(f(x)=x+1\)
    \begin{answer} \(f^{-1}(x)=x-1\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=x^3+1\)
    \begin{answer} \(f^{-1}(x)=\sqrt[3]{x-1}\)
    \end{answer}\end{exercise}
  \endtwocol%

  \noindent Determine if each of the following is a one-to-one function.
  \twocol%
    \begin{exercise}  \(y=x^2+x\)
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise}  \(y=x^3+x\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise}  \(y=x^3-x+1\)
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise}  \(y=\frac{1}{x}\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}
  \endtwocol%
\end{exercises}
