\chapter{Differentiation}

Finally! We are getting into differentiation. Are you excited?

\section{What is derivative?}{}{}

The definition of derivative depends on the concept of a tangent line.
\begin{definition}\index{tangent}
    Informally, a tangent is a line that touches a curve at one point only. 
\end{definition}

One way we can visualize what a tangent is to ``zoom in'' the graph of the function 
until it becomes a straight line. This is illustrated in Figure~\ref{figure:informal-tangent}.
Sometimes the graph never becomes a straight line. This is when we called it non-differentiable.
We will talk differentiability in more details in Chapter~\ref{differentiability}.

\begin{figure*}
\begin{tikzpicture}
	\begin{axis}[
            domain=0:6, range=0:7,
            ymin=-.2,ymax=7,
            width=\textwidth,
            height=7cm, %% Hard coded height! Moreover this effects the aspect ratio of the zoom--sort of BAD
            axis lines=none,
          ]   
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(.8,1.6) (2.834,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(2.834,5) (4.166,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(1.2,1.6) (4.166,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(.8,1.6) (1.2,1.6)} \closedcycle; %% zoom fill

          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(3.3,3.6) (5.334,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(5.334,5) (6.666,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(3.7,3.6) (6.666,5)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(3.3,3.6) (3.7,3.6)} \closedcycle; %% zoom fill
          
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(3.7,2.4) (6.666,1)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(3.3,2.4) (3.7,2.4)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(3.3,2.4) (5.334,1)} \closedcycle; %% zoom fill          
          \addplot [draw=none, fill=background] plot coordinates {(5.334,1) (6.666,1)} \closedcycle; %% zoom fill
          

          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(.8,.4) (2.834,1)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=textColor!10!background] plot coordinates {(2.834,1) (4.166,1)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(1.2,.4) (4.166,1)} \closedcycle; %% zoom fill
          \addplot [draw=none, fill=background] plot coordinates {(.8,.4) (1.2,.4)} \closedcycle; %% zoom fill

          \addplot[very thick,penColor, smooth,domain=(0:1.833)] {-1/(x-2)};
          \addplot[very thick,penColor, smooth,domain=(2.834:4.166)] {3.333/(2.050-.3*x)-0.333}; %% 2.5 to 4.333
          %\addplot[very thick,penColor, smooth,domain=(5.334:6.666)] {11.11/(1.540-.09*x)-8.109}; %% 5 to 6.833
          \addplot[very thick,penColor, smooth,domain=(5.334:6.666)] {x-3}; %% 5 to 6.833
          
          \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(1,1)};  %% point to be zoomed
          \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(3.5,3)};  %% zoomed pt 1
          \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(6,3)};  %% zoomed pt 2

          \addplot [->,textColor] plot coordinates {(0,0) (0,6)}; %% axis
          \addplot [->,textColor] plot coordinates {(0,0) (2,0)}; %% axis
          
          \addplot [textColor!50!background] plot coordinates {(.8,.4) (.8,1.6)}; %% box around pt
          \addplot [textColor!50!background] plot coordinates {(1.2,.4) (1.2,1.6)}; %% box around pt
          \addplot [textColor!50!background] plot coordinates {(.8,1.6) (1.2,1.6)}; %% box around pt
          \addplot [textColor!50!background] plot coordinates {(.8,.4) (1.2,.4)}; %% box around pt
          
          \addplot [textColor!50!background] plot coordinates {(2.834,1) (2.834,5)}; %% zoomed box 1
          \addplot [textColor!50!background] plot coordinates {(4.166,1) (4.166,5)}; %% zoomed box 1
          \addplot [textColor!50!background] plot coordinates {(2.834,1) (4.166,1)}; %% zoomed box 1
          \addplot [textColor!50!background] plot coordinates {(2.834,5) (4.166,5)}; %% zoomed box 1

          \addplot [textColor] plot coordinates {(3.3,2.4) (3.3,3.6)}; %% box around zoomed pt
          \addplot [textColor] plot coordinates {(3.7,2.4) (3.7,3.6)}; %% box around zoomed pt
          \addplot [textColor] plot coordinates {(3.3,3.6) (3.7,3.6)}; %% box around zoomed pt
          \addplot [textColor] plot coordinates {(3.3,2.4) (3.7,2.4)}; %% box around zoomed pt

          \addplot [textColor] plot coordinates {(5.334,1) (5.334,5)}; %% zoomed box 2
          \addplot [textColor] plot coordinates {(6.666,1) (6.666,5)}; %% zoomed box 2
          \addplot [textColor] plot coordinates {(5.334,1) (6.666,1)}; %% zoomed box 2
          \addplot [textColor] plot coordinates {(5.334,5) (6.666,5)}; %% zoomed box 2

          \node at (axis cs:2.2,0) [anchor=east] {$x$};
          \node at (axis cs:0,6.6) [anchor=north] {$y$};
        \end{axis}
\end{tikzpicture}
\caption{Given a function \(f(x)\), if one can ``zoom in''
on \(f(x)\) sufficiently so that \(f(x)\) seems to be a straight line,
then that line is the \textbf{tangent line} to \(f(x)\) at the point
determined by \(x\). (Fowler \& Snapp, 2014)}\label{figure:informal-tangent}
\end{figure*}

Knowing what a tangent is, we can now define derivative. Simply put,
the \textbf{derivative} of a function tells us the slope of that function at
any given point, if it exists. \textbf{Differentiation} is simply the
process of finding the derivative.

Because the slope of any secant line that passes through the points
$(x,f(x))$ and $(x+h, f(x+h))$ is given by
\[
\frac{\Delta y}{\Delta x}=\frac{f(x+h) -f(x)}{(x+h)-x} = \frac{f(x+h)-f(x)}{h},
\]
and because as \(\Delta x \to 0\), the secant line becomes the tangent line,
we can thereby obtain the definition of derivative.

\begin{definition}\index{derivative}
    The \textbf{derivative} of \(f\) at \(x\) is given by
    \[f'(x)=\lim_{\Delta x \to 0}\frac{f(x+\Delta x)-f(x)}{\Delta x}\]
    provided the limit exists. For all \(x\) for which this limit exists, \(f'\)
    is a function of \(x\).
\end{definition}

We can also evaulate the derivative by the following formula,
\[f'(x)=\lim_{x\to c}\frac{f(x)-f(c)}{x-c}\]
which is sometimes called \textbf{the alternative form of the derivative}.

The derivative of a function can be represented in many ways. You might encounter
\(\frac{dy}{dx}\), \(\frac{d}{dx}[f(x)]\), \(y'\), \(f'(x)\), but they are all just
different methods of representing a derivative.

\section{Differentiability}{}{}\label{differentiability}

Much like the case of continuity, the function would be non-differentiable,
when, well, \(\lim_{\Delta x \to 0}\frac{f(x+\Delta x)-f(x)}{\Delta x}\)
fails to exist.

Based on the reason why it does not exist, we can divide non-differentiability
into three main types.
\begin{enumerate}
    \item \(f(x)\) has a \textbf{discontinuity} at \(x=c\). So, \(f(x)\) in the formula does not exist.
    \item \(f(x)\) has a \textbf{sharp turn} at \(x=c\) (e.g.\ a ``corner'' or a ``cusp''), like illustrated in Plot~\ref{plot:non-differentiable-function-sharp-turn}
            In this case, the left-hand limit would be different from the right-hand limit.
    \item \(f(x)\) has a \textbf{vertical tangent} at \(x=c\). In this case, the limit would be infinity.
\end{enumerate}

\begin{marginfigure}[-2in]
    \begin{adjustbox}{width=\marginparwidth}
    \begin{tikzpicture}
        \begin{axis}[
                domain=0:10,
                ymax=5,
                ymin=0,
                samples=100,
                axis lines =middle, xlabel=$x$, ylabel=$y$,
                every axis y label/.style={at=(current axis.above origin),anchor=south},
                every axis x label/.style={at=(current axis.right of origin),anchor=west}
              ]
          \addplot [very thick, penColor, smooth, domain=(5:10)] {x-4};
          \addplot [very thick, penColor, smooth, domain=(0:5)] {-x+6};
            \end{axis}
    \end{tikzpicture}
    \end{adjustbox}
    \caption{Example of a sharp turn non-differentiability}
    \label{plot:non-differentiable-function-sharp-turn}
\end{marginfigure}

And if the function is differentiable, we know it's also continuous!

\begin{theorem}[Differentiability Implies Continuity]\label{theorem:diff-cont}
    If $f(x)$ is a differentiable function at $x = a$, then $f(x)$ is
    continuous at $x=a$. (Fowler \& Snapp, 2014)
\end{theorem}

\section{Derivative Rules}{}{}
The derivative rules! But it could be hard to calculate the derivative using its definition formula
all the time. So mathematicians have came up with many useful formula to make our life easier.

\begin{theorem}[Derivative Rules]\label{theorem:derivative-rules}
    \begin{enumerate}
        \item The Constant Rule: \(\frac{d}{dx}[c]=0\).\index{Constant Rule}
        \item The Power Rule: \(\frac{d}{dx}[x^n]=nx^{n-1}\).\index{Power Rule}
        \item The Constant Multiple Rule: \(\frac{d}{dx}[cf(x)]=cf'(x)\).\index{Constant Multiple Rule}
        \item The Sum and Difference Rules: \(\frac{d}{dx}[f(x)\pm g(x)]=f'(x)\pm g'(x)\)\index{Sum and Difference Rule}
        \item The Product Rule: \(\frac{d}{dx}[f(x)g(x)]=f'(x)g(x)+g'(x)f(x)\)\index{Product Rule}
        \item The Quotient Rule: \(\frac{d}{dx}[\frac{f(x)}{g(x)}]=\frac{f'(x)g(x)-g'(x)f(x)}{[g(x)]^2}\)\index{Quotient Rule}
        \item The Chain Rule: \(\frac{d}{dx}[f(g(x))]=f'(g(x))g'(x)\), or \(\frac{dy}{dx}=\frac{dy}{du} \cdot \frac{du}{dx}\)\index{Chain Rule}
    \end{enumerate}
\end{theorem}

\section{Higher-Order Derivatives}{}{}
Higher-order derivative is when we take the derivative of derivative. It can be useful in many ways.
For example, when we take the derivative of the displacement, we get velocity. And when we take the second-derivative of
the displacement, we get acceleration!
\[a(t)=v'(t)=s''(t)\]

\section{Implicit Differentiation}{}{}
Implicit differentiation is when you differentiate a implicit function.
\begin{definition}\index{explicit/implict function}
An explicit function is an equation where the dependent variable is written in terms of the independent variable.
An implicit function is an equation where the dependent variable is not stated explicitly in terms of the independent variable.
(Fowler \& Snapp, 2014)
\end{definition}

The guideline of for implicit differentiation is listed as the followings. (Ron \& Bruce, 2012)
\begin{enumerate}
\item Differentiate both sides of the equation with respect to \(x\).
\item Collect all terms of \(\frac{dy}{dx}\) to the left side.
\item Factor \(\frac{dy}{dx}\).
\item Solve for \(\frac{dy}{dx}\).
\end{enumerate}

\begin{example}
    Find \(\frac{dy}{dx}\) of \((x+y)^3=8x\).
\end{example}

\begin{solution}
    Let's follow the guideline!
    \begin{enumerate}
        \item Differentiate both sides of the equation with respect to \(x\).
        
        When doing this step, the key point is to think \(y\) as a \textbf{implicit function} of \(x\).
        \begin{align*}
        \frac{dy}{dx}[(x+y)^3]&=\frac{dy}{dx}[8x] \\ 
        3(x+y)^2\frac{dy}{dx}[x+y]&=8 \\ 
        3(x+y)^2(1+\frac{dy}{dx})&=8 \\ 
        3(x+y)^2+3(x+y)^2\frac{dy}{dx}&=8
        \end{align*}
        \item Collect all terms of \(\frac{dy}{dx}\) to the left side.
        \[3(x+y)^2\frac{dy}{dx}=8-3(x+y)^2\]
        \item Factor \(\frac{dy}{dx}\).
        \[(3(x+y)^2)\frac{dy}{dx}=8-3(x+y)^2\]
        \item Solve for \(\frac{dy}{dx}\).
        \[\frac{dy}{dx}=\frac{8-3(x+y)^2}{3(x+y)^2}\]
    \end{enumerate}
\end{solution}
\begin{exercises}
    

    \noindent Find the derivative of the following function using the definition of derivative.
    \begin{exercise} \(f(x)=x^2+x\)
    \begin{answer} \begin{align*}
        \frac{d}{dx}[f(x)]
        &=\lim_{\dx\to 0}\frac{f(x+\dx)-f(x)}{\dx}\\
        &=\lim_{\dx\to 0}\frac{(x+\dx)^2+(x+\dx)-(x^2+x)}{\dx}\\ 
        &=\lim_{\dx\to 0}\frac{x^2+\dx^2+2x\dx+\dx-x^2-x}{\dx}\\ 
        &=\lim_{\dx\to 0}\frac{\dx^2+\dx+2x\dx}{\dx}\\
        &=\lim_{\dx\to 0}(\dx+1+2x)\\
        &=2x+1
    \end{align*}
    \end{answer}\end{exercise}
    
    \noindent Find the derivative of the following function using the alternative form of derivative.
    \begin{exercise} \(f(x)=x^3-x\)
    \begin{answer} \begin{align*}
        \frac{d}{dx}[f(x)]
        &=\lim_{x\to c}\frac{f(x)-f(c)}{x-c}\\
        &=\lim_{x\to c}\frac{x^3-x-c^3+c}{x-c}\\ 
        &=\lim_{x\to c}\frac{(x-c)(x^2 + xc + c^2)-(x-c)}{x-c}\\
        &=\lim_{x\to c}((x^2 + xc + c^2)-1)\\
        &=c^2+c^2+c^2-1\\
        &=3c^2-1
    \end{align*}
    \end{answer}\end{exercise}
  
    \noindent Determine whether the following functions are differentiable everywhere. If not, identify the type of non-differentiability.
    \begin{exercise} \(f(x)=x^2+\frac{1}{x}\)
    \begin{answer} No. There is a discontinuity at \(x=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{(x+1)(x-2)}{x-2}\)
    \begin{answer} No. There is a discontinuity at \(x=2\).
    \end{answer}\end{exercise}
      
    \begin{exercise} \(f(x)=|x|\)
    \begin{answer} No. There is a corner at \(x=0\).
    \end{answer}\end{exercise}
      
    \begin{exercise} \(f(x)=x^2+\sqrt[3]{x}\)
    \begin{answer} No. There is a vertical tangent at \(x=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=x^2+\sqrt{|x|}\)
    \begin{answer} No. There is a cusp at \(x=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=x^3-2x+3\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}
  
    \noindent Find the derivative of the following functions.
    \begin{exercise} \(f(x)=-x^3+2x^2-3x+5\)
    \begin{answer} \(f'(x)=-3x^2+4x-3\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=x+\frac{2}{x}\)
    \begin{answer} \(f'(x)=1-\frac{2}{x^2}\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=(2x+1)(x-c)\), \(c\) is a constant
    \begin{answer} \(f'(x)=(2x+1)(1)-(x-c)(2)=x+1-c \)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{3x^2+1}{x-5}\)
    \begin{answer} \(f'(x)=\frac{(x-5)(6x)-(3x^2+1)(1)}{(x-5)^2}=\frac{3x^2-30x-1}{(x-5)^2} \)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=(x^2+2x+3)^5\)
    \begin{answer} \(f'(x)=5(2x+2)(x^2+2x+3)^4\)
    \end{answer}\end{exercise}

    \begin{exercise} \(x^2+y^2=1\)
    \begin{answer} \(2x+2y\dydx=0\), \(\dydx=\frac{-x}{y}\)
    \end{answer}\end{exercise}

    \begin{exercise} \(x^2+xy^3+y=1\)
    \begin{answer} \(2x+x(3y^2)\dydx+y^3+\dydx=0\), \(\dydx=\frac{-y^3-2x}{3xy^2+1}\)
    \end{answer}\end{exercise}

    \noindent Find the second derivative of the following functions.
    \begin{exercise} \(f(x)=x^3+2x^2-5x+7\)
    \begin{answer} \(f''(x)=6x+4\)
    \end{answer}\end{exercise}

    \begin{exercise} \(x^2+y^3=1\)
    \begin{answer} \(2x+3y^2\dydx=0\), \(\dydx=\frac{-2x}{3y^2}\),
        \begin{align*}
            \frac{dy^2}{dx^2}&=\frac{(3y^2)(-2)-(-2x)(6y\dydx)}{9y^4}\\ 
                             &=\frac{-6y^2+12xy\frac{-2x}{3y^2}}{9y^4}\\ 
                             &=\frac{-6y^2-\frac{24x^2}{3y}}{9y^4}\\ 
                             &=\frac{-6y^2}{9y^4}-\frac{24x^2}{27y^5}\\ 
                             &=\frac{-6y^3}{9y^5}-\frac{8x^2}{9y^5}\\
                             &=\frac{-6y^3-8x^2}{9y^5}
        \end{align*}
    \end{answer}\end{exercise}
\end{exercises}
  