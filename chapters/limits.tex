\chapter{Limits and Continuity}

Limits play a fundamental role in both differentiation and integration.
Thus, it is very important for one to understand the concept of limits.

\section{What is limits?}{}{}\index{limit}
Let's consider the function \[f(x)=\frac{x^2-1}{x+1}\]
Looking at the plot~\ref{plot:limits}, we can notice that the function
has a hole at (-1, -2).
Now in calculus, we call it a \textbf{removable discontinuity}. We
will talk more about continuity in Section~\ref{continuity} later.

Closely examining the Plot~\ref{plot:limits} and the Table~\ref{table:(x^2-1)/(x+1)},
we see that as \(x\) approaches -1, \(f(x)\) approaches -2. We can
represent this using the concept of limit:
\[ \lim_{x\to -1} f(x)=-2 \]

\begin{definition}
Intuitively, $\lim_{x\to a} f(x) = L$ when the value of $f(x)$ can
be made arbitrarily close to $L$ by making $x$ sufficiently close, but
not equal to, $a$. (Fowler \& Snapp, 2014)
\end{definition}

The formal definition, which is often refered to as the epsilon-delta ($\epsilon-\delta$)
definition, will not be covered in this book. But it is just a more rigorous
statement of the above intutive description.

\begin{marginfigure}[-5in]
    \begin{adjustbox}{width=\marginparwidth}
\begin{tikzpicture}
	\begin{axis}[
            domain=-3:3,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            xtick={-3,...,3},
            ytick={-3,...,3},
          ]
	  \addplot [very thick, penColor, smooth] {x-1};
          \addplot[color=penColor,fill=background,only marks,mark=*] coordinates{(-1,-2)};  %% open hole
        \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{A plot of \(f(x)=\protect\frac{x^2-1}{x+1}\)}\label{plot:limits}  
\end{marginfigure}
\begin{margintable}[-1in]
    \[
    \begin{tchart}{ll}
     x & f(x) \\ \hline
     -1.1 & -2.1 \\
     -1.01 & -2.01 \\
     -1.001 & -2.001 \\
     -1.0001 &  -2.0001 \\
      -1 &  \text{undefined} \\
     -0.9999 &  -1.9999 \\
     -0.999 &  -1.999 \\
     -0.99 &  -1.99 \\
     -0.9 &  -1.9 \\
    \end{tchart}
    \]
    \caption{Values of \(f(x)=\protect\frac{x^2-1}{x+1}\).}\label{table:(x^2-1)/(x+1)}
\end{margintable}

\begin{warning}
    Note that when evaluating the limit \(\lim_{x\to c} f(x)\), it does not matter whether the
    function is actually defined or not at $x=c$ (nor does it matter
    the actual value when $x=c$), as $x$ is only
    \textbf{approaching} but never actually at the point $c$.
\end{warning}

\section{Left-hand and Right-hand limits}{}{}

\begin{definition}\index{limit!one-sided limits}
    \(\lim_{x\to c^-} f(x)=L\) represents what happens to $f(x)$ as it
    approaches $c$ from the \textbf{left side} of the value. It is
    sometimes called the left-hand limit.

    \(\lim_{x\to c^+} f(x)=L\) represents what happens to $f(x)$ as it
    approaches $c$ from the \textbf{right side} of the value. It is
    sometimes called the right-hand limit.
\end{definition}

\begin{theorem}[Limit Existence]\label{theorem:limit-existence} 
    if \(\lim_{x\to c^-} f(x)=\lim_{x\to c^+} f(x)=L\), that is
    the right-side limit and the left-side limit both exists, and are the
    same, \(\lim_{x\to c} f(x)=L\). Otherwise, the limit does not exists.
\end{theorem}

To better understand the left-hand and right-hand limits, we can take
a look at the plot~\ref{plot:rl_limit_plot}. When we approaches \(x=0\)
from the left side, we can see that \(y\) approaches 0. But when approaching
from the right side, \(y\) approaches 1. Thus, the following conclusions can
be made,
\[
    \begin{aligned}
    \lim_{x\to 0^-} f(x)&=0 \\ 
    \lim_{x\to 0^+} f(x)&=1 \\ 
    \lim_{x\to 0} f(x) &\textit{ does not exist}
    \end{aligned}
\]

\begin{marginfigure}[-1in]
    \begin{adjustbox}{width=\marginparwidth}
\begin{tikzpicture}
	\begin{axis}[
            domain=-2:3,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            xtick={-2,...,3},
            ytick={-3,...,3},
          ]
	  \addplot [very thick, penColor, smooth, domain=-2:0] {x^2};
	  \addplot [very thick, penColor, smooth, domain=0:3] {x^(1/2)+1};
        \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{A plot of a function}\label{plot:rl_limit_plot}  
\end{marginfigure}


\section{Continuity}{}{}\label{continuity}\index{continuity}
Informally, a function is continuous if you can ``draw it'' without ``lifting your pen.''
\begin{definition}
    A function f is continuous at a point a if \(\lim_{x\to a}f(x)=f(a)\).
\end{definition}
From the definition of continuity, we can divide discontinuity into two main categories.
\begin{enumerate}
\item Removable (point) discontinuities: these are discontinuity that can be removed by
defining or redefining a point at the function.

Namely, either of the following is true when there is a removable discontinuity.
\begin{itemize}
    \item \(\lim_{x\to a}f(x)\) exists but does not equal to \(f(a)\).
    \item \(\lim_{x\to a}f(x)\) exists but \(f(a)\) does not exists.
\end{itemize}
In either case, we can make the function continuous by simply defining/redefining the function
so that \(\lim_{x\to a}f(x)=f(a)\).

\item Non-removable (jump) discontinuities: this occurs when the left-hand limits does not equal
to the right-hand limit, that is, when \(\lim_{x\to a}f(x)\) does not exists at all!
We cannot remove it by simply adding a point, so it is called \textbf{non-removable discontinuity}.
\end{enumerate}

\begin{marginfigure}[0in]
    \begin{adjustbox}{width=\marginparwidth}
    \begin{tikzpicture}
        \begin{axis}[
                domain=0:10,
                ymax=5,
                ymin=0,
                samples=100,
                axis lines =middle, xlabel=$x$, ylabel=$y$,
                every axis y label/.style={at=(current axis.above origin),anchor=south},
                every axis x label/.style={at=(current axis.right of origin),anchor=west}
              ]
          \addplot [very thick, penColor, smooth, domain=(4:10)] {3 + sin(deg(x*2))/(x-1)};
              \addplot [very thick, penColor, smooth, domain=(0:4)] {1};
              \addplot[color=penColor,fill=background,only marks,mark=*] coordinates{(4,3.30)};  %% open hole
              \addplot[color=penColor,fill=background,only marks,mark=*] coordinates{(6,2.893)};  %% open hole
              \addplot[color=penColor,fill=background,only marks,mark=*] coordinates{(8,2.893)};  %% open hole
              \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(4,1)};  %% closed hole
              \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(6,2)};  %% closed hole
            \end{axis}
    \end{tikzpicture}
    \end{adjustbox}
    \caption{A plot of a function with discontinuities. (Fowler \& Snapp, 2014)}
    \label{plot:discontinuous-function}
\end{marginfigure}

\begin{example}
    The plot~\ref{plot:discontinuous-function} can be used as an example to illustrate different
    types of discontinuities.
    There is a non-removable discontinuity at \(x=4\) because the right-hand limit does not equal to the
    left-hand limit.
    \[
        \lim_{x\to 4^{-}}f(x)=1 \textit{and} \lim_{x\to 4^{+}}f(x)\approx 3.4
    \]
    
    \[
        \therefore \lim_{x\to 4^{-}}f(x) \neq \lim_{x\to 4^{+}}f(x)
    \]
    
    There are also removable discontinuities at \(x=6\) and \(x=8\), where the limit does not equal to the
    actual value (\(\lim_{x\to 6}f(x) \neq f(6)\)) and the value is not defined (\(f(8)\) does not exist).           
\end{example}

From the definition of \textsl{continuous} at a point, we can now define continuity on a interval.

\begin{definition}
    A function is \textbf{continuous on an open interval (a, b)} if it is continuous at each point in the interval.
    (CITATION NEEDED)
\end{definition}

\begin{definition}
    A function is \textbf{continuous on an closed interval [a, b]} if it is continuous on the open interval (a,b)
    and
    \[\lim_{x\to a^{+}} f(x)=f(a) \textit{ and } \lim_{x\to b^{-}} f(x)=f(b)\]
    The function \(f\) is \textbf{continuous from the right} at a and \textbf{continuous from the left} at b.
    (CITATION NEEDED)
\end{definition}

From the concept of continuity, we can have a useful theorem called the Intermediate value theorem,
described as the following,

\begin{theorem}[Intermediate Value Theorem]\label{theorem:IVT}
    If $f(x)$ is a continuous function for all $x$ in the closed interval
    $[a,b]$ and $d$ is between $f(a)$ and $f(b)$, then there is a number
    $c$ in $[a, b]$ such that $f(c) = d$. (Fowler \& Snapp, 2014)
\end{theorem}

It looks very apparent and intuitive - you cannot draw a continuous line from one side
of the line to the other side without crossing the line, although the rigorous proof is not
that obvious.

\section{Evaluating Limits Analytically}{}{}
Okay, okay, I get it. Limits are good and useful. But how to evaulate them? Do we have to graph them first?

Not anymore with the help of the following theorems!
\begin{theorem}[Some Basic Limits]\label{theorem:basic-limits} 
    Let \(b\) and \(c\) be real numbers, and let \(n\) be a positive integer.
    \begin{enumerate}
        \item \(\lim_{x\to c}b=b\)
        \item \(\lim_{x\to c}x=c\)
        \item \(\lim_{x\to c}x^n=c^n\)
    \end{enumerate}
    (CITATION NEEDED)
\end{theorem}
\begin{theorem}[Properties of Limits]\label{theorem:limits-properties} 
    Let \(b\) and \(c\) be real numbers, let \(n\) be a positive integer,
    and let \(f\) anf \(g\) be functions with the following limits,
    \[\lim_{x\to c}f(x)=L \textit{ and } \lim_{x\to c}g(x)=R\]
    \begin{enumerate}
        \item \(\lim_{x\to c}[bf(x)]=bL\)
        \item \(\lim_{x\to c}[f(x)\pm g(x)]=L\pm R\)
        \item \(\lim_{x\to c}[f(x)g(x)]=LR\)
        \item \(\lim_{x\to c}[\frac{f(x)}{g(x)}]=\frac{L}{R}\), provided \(K\neq 0\)
        \item \(\lim_{x\to c}[f(x)^n]=L^n\)
        \item \(\lim_{x\to c}[\sqrt[n]{f(x)}]=\sqrt[n]{L}\)
    \end{enumerate}
    (CITATION NEEDED)
\end{theorem}
\begin{theorem}[Limit of A Composite Function]\label{theorem:limits-composite} 
    If \(f\) and \(g\) are functions such that \(\lim_{x\to c} g(x)=L\) and \(\lim_{x\to L} f(x)=f(L)\)
    (i.e. \(f\) is continous at \(x=L\)) 
    \[\lim_{x\to c} f(g(x))=f(\lim_{x\to c}g(x))=f(L)\]
    (CITATION NEEDED)
\end{theorem}
There are a lot theorems! But luckily, they are all pretty intuitive.

There also also some strategies for finding limits. Simply put, when you
meet a limit, you should try the \textbf{direct substitution} method first,
which is, just substitute all the \(x\) with \(c\). If that gives you
\textbf{indeterminate forms}, such as \(\frac{0}{0}\), try to use methods
such as factoring, dividing out, rationalization to find the limit.


\section{The Squeeze Theorem}{}{}
The squeeze theorem is another approach to find limits.

\begin{theorem}[The Squeeze Theorem]\label{theorem:squeeze-theorem} 
    If \(h(x)\le f(x)\le g(x)\) for all x in an open interval containing \(c\),
    except possibly at \(c\) itself, and if \(\lim_{x\to c} h(x)=L=\lim_{x\to c} g(x)\),
    then \(\lim_{x\to c} f(x)\) exists, and is equal to L.
    (CITATION NEEDED)
\end{theorem}

The squeeze theorem can be used to evaluate many subtle limits.
The plot~\ref{plot:squeeze} is an example of the squeeze theorem as the proof of the limit,
\[\lim_{x\to 0}\frac{\sin(x)}{x}\]
As shown in the plot, there are three functions graphed, namely \(y=1\), \(\frac{\sin(x)}{x}\), and \(cos(x)\).
At around \(x=0\), it can be proven that,
\[\cos(x)\le \frac{\sin(x)}{x} \le 1\]
According to the squeeze, let \(h(x)=\cos(x)\), \(g(x)=1\), because \(h(x)\le f(x)\le g(x)\) for all x in an open interval containing \(x=0\),
\[\lim_{x\to 0}\frac{\sin(x)}{x}=\lim_{x\to 0} g(x)=1\]

It is sometimes refered to as the sandwich theorem, which is straight-forward - function \(h\) and \(g\) are like
the slices of bread of the sandwitch that ``squeeze'' the meat - the function \(f\).

\begin{marginfigure}[-5in]
    \begin{adjustbox}{width=\marginparwidth}
    \begin{tikzpicture}
        \begin{axis}[
                domain=-5:5,
                ymax=5,
                ymin=-5,
                samples=100,
                axis lines =middle, xlabel=$x$, ylabel=$y$,
                every axis y label/.style={at=(current axis.above origin),anchor=south},
                every axis x label/.style={at=(current axis.right of origin),anchor=west}
              ]
          \addplot [very thick, penColor, smooth, domain=(-5:5), samples=100] {cos(deg(x))};
          \addplot [very thick, penColor2, smooth, domain=(-5:5), samples=100] {sin(deg(x))/x};
          \addplot [very thick, penColor3, smooth, domain=(-5:5)] {1};
          \addplot[color=penColor2,fill=background,only marks,mark=*] coordinates{(0,1)};  %% open hole
          
          \node at (axis cs:3,-1.5) [anchor=west] {\color{penColor}$h(x)=\cos x$};  
          \node at (axis cs:2,0.5) [anchor=west] {\color{penColor2}$f(x)=\frac{\sin x}{x}$};
          \node at (axis cs:3,1.5) [anchor=west] {\color{penColor3}$g(x)=1$};
        \end{axis}
    \end{tikzpicture}
    \end{adjustbox}
    \caption{An application of the squeeze theorem}
    \label{plot:squeeze}
\end{marginfigure}

\section{Infinite Limits}{}{}\index{limit!infintie limits}
Lastly, we will combine the concepts of asymptotes and limit to wrap up this chapter. 
\begin{definition}
    A function f has an infinite limit if \(\lim_{x\to c}f(x)=\pm\infty\).
\end{definition}
\begin{warning}
    Note that an infintie limit does not mean the limit exists. Instead,
    it is just a special case where the limit \textbf{does not exist}.
\end{warning}
\begin{theorem}[Vertical Asymptotes]\label{theorem:vertical-asymptotes}\index{asymptote!vertical asymptote}
    Informally, a function has a vertical asymptote at \(x=c\) if
    \[\lim_{x\to c^{+}}f(x)=\pm\infty \textit{ or } \lim_{x\to c^{-}}f(x)=\pm\infty\]
\end{theorem}

\begin{definition}\index{asymptote!horizontal asymptote}
    An horizontal (and oblique) asymptote is a line that the
    graph approaches but does not touch as \(x\) approaches \(\pm \infty\).
\end{definition}

\begin{theorem}[Horizontal Asymptotes]\label{theorem:horizontal-asymptotes} 
    If \[\lim_{x\to \infty}f(x)=L\] or \[\lim_{x\to -\infty}f(x)=L\]
    then \(y=L\) is a horizontal asymptote.
\end{theorem}

\begin{exercises}

    \noindent Evaulate the following limits.
    \begin{exercise} \(\lim_{x\to 2}\frac{3x^2+3x}{x^3-2}\)
    \begin{answer} \(3\)
    \end{answer}\end{exercise}
  
    \begin{exercise} \(\lim_{x\to -1}\frac{x^2+3x+2}{x^2-1}\)
    \begin{answer} \(-\frac{1}{2}\)
    \end{answer}\end{exercise}

    \begin{exercise} \(\lim_{x\to 0}\frac{\sin(5x)}{2x}\)
    \begin{answer} \(\frac{5}{2}\)
    \end{answer}\end{exercise}
  
    \begin{exercise} \(\lim_{x\to 0}\frac{1}{x}\)
    \begin{answer} The limit does not exist.
    \end{answer}\end{exercise}

    \begin{exercise} \(\lim_{x\to 0^{-}}\frac{1}{x}\)
    \begin{answer} \(-\infty\). The limit does not exist.
    \end{answer}\end{exercise}
    
    \begin{exercise} \(\lim_{x\to \infty}\frac{3x^2+2x+3}{5x^2+8}\)
    \begin{answer} \(\frac{3}{5}\)
    \end{answer}\end{exercise}
      
    \begin{exercise} \(\lim_{x\to -\infty}\frac{5x^3-9x+8}{5x^2+x}\)
    \begin{answer} \(-\infty\)
    \end{answer}\end{exercise}

    \begin{exercise} \(\lim_{x\to \infty}x\sin \frac{1}{x}\) (Bock, Donovan, \& Hockett, 2017)
    \begin{answer} \(\lim_{x\to \infty}x\sin \frac{1}{x} = \lim_{x\to \infty}\frac{\sin \frac{1}{x}}{\frac{1}{x}}=1\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\begin{cases} 
        2x & x \neq 10 \\
        10 & x = 10
      \end{cases}\)

      \(\lim_{x\to 10}f(x)\)
    \begin{answer} \(20\)
    \end{answer}\end{exercise}
  
    \begin{exercise} \(f(x)=\begin{cases} 
        5 & x \ge 0 \\
        -5 & x < 0
      \end{cases}\)

      \(\lim_{x\to 0}f(x)\)
    \begin{answer} the limit does not exist
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\begin{cases} 
        5 & x \ge 0 \\
        -5 & x < 0
      \end{cases}\)

      \(\lim_{x\to 0^{+}}f(x)\)
    \begin{answer} \(5\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\begin{cases} 
        5 & x \ge 0 \\
        -5 & x < 0
      \end{cases}\)

      \(\lim_{x\to 0^{-}}f(x)\)
    \begin{answer} \(-5\)
    \end{answer}\end{exercise}

    \noindent Determine whether the following functions are continuous. If not, identify the type of discontinuity.
    \begin{exercise} \(f(x)=\begin{cases} 
        5 & x \ge 0 \\
        -5 & x < 0
      \end{cases}\)
    \begin{answer} No, there is a non-removable discontinuity at \(x=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=|x|\)
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\begin{cases} 
        x & x \neq 0 \\
        0 & x = 5
      \end{cases}\)
    \begin{answer} No, there is a removable discontinuity at \(x=5\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{1}{x}\)
    \begin{answer} No, there is a non-removable discontinuity at \(x=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{x^2-1}{x+1}\)
    \begin{answer} No, there is a removable discontinuity at \(x=-1\).
    \end{answer}\end{exercise}

    \begin{exercise} If \(f(x)=\begin{cases} 
        \frac{x^2-x}{2x} & x \neq 0 \\
        k & x = 0
      \end{cases}\), and if \(f\) is continuous at \(x=0\), then what is the value of \(k\)? (Bock, Donovan, \& Hockett, 2017)
    \begin{answer} -\(\frac{1}{2}\)
    \end{answer}\end{exercise}

    \noindent Let \(f(x)=\begin{cases} 
        \frac{x^2+x}{x} & x \neq 0 \\
        1 & x=0
      \end{cases}\). (Bock, Donovan, \& Hockett, 2017)
      
    \begin{exercise} Does \(f(0)\) exist?
    \begin{answer} Yes
    \end{answer}\end{exercise}
    
    \begin{exercise} Does \(\lim_{x\to 0}f(x)\) exist?
    \begin{answer} Yes
    \end{answer}\end{exercise}

    \begin{exercise} Is \(f\) continuous at \(x=0\)?
    \begin{answer} Yes
    \end{answer}\end{exercise}
\end{exercises}
  