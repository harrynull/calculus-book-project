\chapter{Applications of Differentiation}

So what kind of problems can we solve with the knowledge of differentiation?
In this chapter, we are going to explore the ways that it can be applied to
solve math or real-life problems.

\section{Extrema}{}{}\index{extrema}

One of the functions of differentiation is that it can help us find the
absolute/relative minimum/maximum of a function without plotting its graph.
This will be useful in many optimization problems.

\begin{definition}\index{extrema!absolute maximum}
    Absolute maximum, or global maximum, is the \textbf{highest point} on the graph \(f(x)\).
    \(f(d)\) is the absolute maximum if \(f(d)\ge f(x)\) for all \(x\) in \(f(x)\).
\end{definition}

\begin{definition}\index{extrema!absolute minimum}
    Absolute minimum, or global minimum, is the \textbf{lowest point} on the graph \(f(x)\).
    \(f(d)\) is the absolute minimum if \(f(d)\le f(x)\) for all \(x\) in \(f(x)\).
\end{definition}

\begin{definition}\index{extrema!relative maximum}
    Relative maximum, or local maximum, is the \textbf{highest point} on the graph \(f(x)\) compared
    to the \textbf{neighboring domain}.
\end{definition}

\begin{definition}\index{extrema!relative minimum}
    Relative minimum, or local minimum is the \textbf{lowest point} on the graph \(f(x)\) compared
    to the \textbf{neighboring domain}.
\end{definition}

Note that minima is just the plural form of minimum, like maxima is the plural for 
maximum, and extrema is the plural for extremum.

\begin{warning}
    One might be tempted to think that the difference between relative and 
    absoulte extrema is that relative extrema are with regard to an interval,
    while absoulte extrema are talking about the whole function.

    \textbf{This is actually not true.} We can have absolute extrema on
    a certain interval, and have relative extrema in the domain of the 
    whole function.

    The real difference between absolute and relative extrema is that,
    relative extrema are always \textbf{critical points} (See Chapter~\ref{critical-points}),
    while absolute extrema can also be at the endpoints of the interval,
    as long as they are the highest/lowest point for the given interval.
\end{warning}

\begin{example}
    Identify the extrema in Plot~\ref{plot:extrema-function}. 
\end{example}
\begin{solution}
    \((-0.7,0.4)\) is a relative maximum;
    \((0,0)\) is a relative minimum;
    \((1.4,2.8)\) is both a relative maximum and an absolute maximum.
\end{solution}

\begin{example}
    Identify the extrema in Plot~\ref{plot:extrema-function} \textbf{in the interval [-1,1]}. 
\end{example}
\begin{solution}
    \((-0.7,0.4)\) is a relative maximum;
    \((0,0)\) is both a relative minimum and an absolute minimum;
    \((-1,0)\) is an absolute minimum.
    \((1,2)\) is an absoulte maximum.

    Note that we can have an absoulte extrema even there is a interval given.
    Also, \((0,0)\) now becomes an absolute minimum, because \(0\) is the smallest value
    in the interval \([-1,1]\).
    Lastly, notice that \((-1,0)\) and \((1,2)\) are absolute extrema even though they are
    not critical points (See Chapter~\ref{critical-points}) and their slopes are not 0.
\end{solution}

\begin{marginfigure}[-5in]
    \begin{adjustbox}{width=\marginparwidth}
\begin{tikzpicture}
	\begin{axis}[
            domain=-3:3,
            ymax=5,ymin=-2,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            xtick={-3,...,3},
            ytick={-3,...,3},
          ]
	  \addplot [very thick, penColor, smooth] {-1*x^4 + x^3 + 2*x^2};
      \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(-0.69,0.4)};  %% closed hole
      \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(0,0)};  %% closed hole
      \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(1.44,2.83)};  %% closed hole
    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{Plot of \(f(x)=-x^4+x^3+2x^2\)}\label{plot:extrema-function}  
\end{marginfigure}

\section{Critical Points}{}{}\label{critical-points}
\begin{definition}\index{critical point}
    A critical point is a point on the graph of \(f(x)\) where \(f'(x)=0\) or \(f'(x)\) is \textit{undefined}.
\end{definition}
\begin{definition}\index{citical number}
    A critical number is a number \(c\) \textbf{in the domain of \(f(x)\)} where \(f'(x)=0\) or \(f'(x)\) is \textit{undefined}.
\end{definition}

\begin{warning}
    Always ensure that the critical number is in the domain of the function.
\end{warning}

\begin{warning}
    Some people might neglict to consider the case when \(f'(x)\) is \textit{undefined},
    leading to incomplete answers.

    Some are also tempted to think that when the \(f(x)\) is \textit{undefined},
    \(f'(x)\) must also be \textit{undefined}. This is not true.

    Remember to always consider both the case when \(f'(x)=0\) and
    when \(f'(x)\) is \textit{undefined}.

    The Plot~\ref{plot:maximum-undefined-derivative} gives an example where a maximum occurs at where
    \(f'(x)\) is \textit{undefined}. The function is not differentiable at 
    \(x=0\) because of the corner, but nevertheless (0,3) is a relative and
    absolute maximum.
\end{warning}

\begin{marginfigure}[0in]
    \begin{adjustbox}{width=\marginparwidth}
\begin{tikzpicture}
	\begin{axis}[
            domain=-3:3,
            ymax=5,ymin=-2,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            xtick={-3,...,3},
            ytick={-2,...,5},
          ]
	  \addplot [very thick, penColor, smooth] {-abs(x)+3};
      \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(0,3)};  %% closed hole
    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{Plot of \(-\left|x\right|+3\)}\label{plot:maximum-undefined-derivative}  
\end{marginfigure}

Lastly, we need to introduce a theorem that is very important for finding extrema.
\begin{theorem}
    All relative extrema occur at critical points.
\end{theorem}

\begin{warning}
    Note that the converse of the above theorem is not necessarily true. That is, \textbf{not
    all critical points are relative extrema}.
\end{warning}

\section{Find Absolute Extrema Algebraically}{}{}
Knowing what critical points are, we can now find extrema algebraically.
To find the extrema of a continuous function \(f\) on a closed interval \([a,b]\),

\begin{enumerate}
    \item Find \(f'(x)\); determine the critical numbers, \(c\).
    \item Evaluate for all \(f(c)\).
    \item Evaluate your end points: \(f(a)\) and \(f(b)\).
    \item Compare values from step 2 and 3 to determine extrema.
\end{enumerate}

\begin{example}
    Find all absolute extrema in \(f(x)=-x^4+x^3+2x^2\) in the interval \([-1,1]\). 
\end{example}
\begin{solution}
    Let's follow the steps listed above.
    \begin{enumerate}
        \item Find \(f'(x)\); determine the critical numbers, \(c\).
        \[f(x)=-x^4+x^3+2x^2\]
        \[f'(x)=-4x^3+3x^2+4x=x(-4x^2+3x+4)\]
        To determine the critical numbers, as the function is defined everywhere, we need to find
        \(x\) that can make \(f'(x)=0\).
        \begin{align*}
        f'(x)&=0 \\ 
        x(-4x^2+3x+4)&=0 \\ 
        x_1&=0, \\
        x_2&=\frac{3+\sqrt{73}}{8} \approx 1.44, \\
        x_3&=\frac{3-\sqrt{73}}{8} \approx-0.69
        \end{align*}
        \(x_2\) is neglicted because it is not in the interval \([-1,1]\).
        \item Evaluate for all \(f(c)\).
        
        \begin{align*}
            f(0)&=0 \\
            f(\frac{3-\sqrt{73}}{8})& \approx 0.4
        \end{align*}

        \item Evaluate your end points: \(f(a)\) and \(f(b)\).
        \begin{align*}
            f(-1)&=0 \\
            f(1)&=2
        \end{align*}
        \item Compare values from step 2 and 3 to determine extrema.
        Thus, \((0,0)\), \((-1,0)\) are absolute minima;
        \((1,2)\) is an absoulte maximum.
    \end{enumerate}
\end{solution}

\section{Find Relative Extrema Algebraically}{}{}
We can apply \textbf{First Derivative Test} or \textbf{Second Derivative Test}
to test whether a critical point is an extremum and what type of extremum it is.

\subsection{First Derivative Test}

Using the fact that
\begin{itemize}
\item If \(f'(x) >0\) on an interval, then \(f(x)\) is increasing on that interval.
\item If \(f'(x) <0\) on an interval, then \(f(x)\) is decreasing on that interval.
\end{itemize}

we can apply a technic called \textit{first derivative test} to determine whether there is a maximum,
minimum, or neither at a point using the derivative.

\begin{theorem}[First Derivative Test]\index{first derivative test}\label{T:fdt}\hfil
Suppose that $f(x)$ is continuous on an interval, and that $f'(a)=0$
for some value of $a$ in that interval.
\begin{itemize}
\item If $f'(x)>0$ to the left of $a$ and $f'(x)<0$ to the right of
  $a$, then $f(a)$ is a local maximum.
\item If $f'(x)<0$ to the left of $a$ and $f'(x)>0$ to the right of
  $a$, then $f(a)$ is a local minimum.
\item If $f'(x)$ has the same sign to the left and right of $a$,
  then $f(a)$ is not a local extremum.
\end{itemize}
(Fowler \& Snapp, 2014)
\end{theorem} 

\begin{warning}
    Note that even we rely on critical points to find change from increasing to decreasing or
    decreasing to increasing, and thereby extrema, please be aware that the function could
    still change increasing/decreasing \textbf{without a critical point}. For example, when
    there is an vertical asymptote.
\end{warning}

\subsection{Second Derivative Test}
To know how to use the second derivative test, we need first define ``concavity''.

\begin{definition}\index{concavity}
    A graph is concave upward when its slope, \(f'(x)\), is increasing, that is when \(f''(x)>0\).
    A graph is concave downward when its slope, \(f'(x)\), is decreasing, that is when \(f''(x)<0\).
\end{definition}

If at the critical number, the graph is concave upward, that means the \(f'(x)=0\) and \(f'(x)\) is increasing.
We can thus conclude that \(f'(x)\) goes from negative to positive, thereby the function going from decreasing
to increasing. Similar conclusion can be made when the graph is concave downward. Thus, we can use the second
derivative of the function to test whether a critical point is a maximum or minimum.

\begin{theorem}[Second Derivative Test]\index{second derivative test}\label{T:sdt}
    Suppose that $f''(x)$ is continuous on an open interval and that
    $f'(a)=0$ for some value of $a$ in that interval.
    \begin{itemize}
    \item If $f''(a)<0$, then $f(x)$ has a local maximum at $a$.
    \item If $f''(a)>0$, then $f(x)$ has a local minimum at $a$.
    \item If $f''(a)=0$, then the test is \textbf{inconclusive}. In this case,
      $f(x)$ may or may not have a local extremum at $x=a$.
    \end{itemize}
    (Fowler \& Snapp, 2014)
\end{theorem}
    
\begin{warning}
    If the second derivative test fails, that is, when \(f''(a)=0\),
    we must fall back on the first derivative test.
\end{warning}

\section{Point of Inflection}
\begin{definition}\index{point of inflection}
    Point of Inflection is a point where the graph's concavity changes. 
    This occurs where \(f''(x)=0\) or \(f''(x)\) does not exist.
\end{definition}

\begin{warning}
    Note that the converse of the above theorem is not necessarily true.
    That is, even if \(f''(x)=0\) or \(f''(x)\) does not exist, it is not
    guaranteed that there is a point of inflection.

    This is because the point of inflection is a point where the concavity
    changes, or the \(f''(x)\) crosses the x-axis (i.e. change sign).
    It is, however, possible that \(f''(x)\) only touches the x-axis but
    does not cross it. See Plot~\ref{plot:same-concavity} for an example.
    It can be seen that even though \(f''(0)=0\), the concavity of \(f(x)\)
    is the same in \((-\infty, 0)\) and \((0, \infty)\), namely both being
    concave upward.

    Thus, always verify both side of potential inflection point to confirm,
    just like we do in the first derivative test.
\end{warning}


\begin{marginfigure}[-2in]
    \begin{adjustbox}{width=\marginparwidth}
\begin{tikzpicture}
	\begin{axis}[
            domain=-3:3,
            ymax=5,ymin=-2,
            axis lines =middle, xlabel=$x$, ylabel=$y$,
            every axis y label/.style={at=(current axis.above origin),anchor=south},
            every axis x label/.style={at=(current axis.right of origin),anchor=west},
            grid=both,
            grid style={dashed, gridColor},
            xtick={-3,...,3},
            ytick={-2,...,5},
          ]
	  \addplot [very thick, penColor, smooth] {x^2};
	  \addplot [very thick, penColor2, smooth] {x^4/12+1};
      \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(0,0)};  %% closed hole
      \node at (axis cs:1,4) [anchor=west] {\color{penColor}$f''(x)=x^2$};
      \node at (axis cs:1,1) [anchor=west] {\color{penColor2}$f(x)=\frac{1}{12}x^4+1$};
    \end{axis}
\end{tikzpicture}
\end{adjustbox}  
\caption{Example where concavity does not change}\label{plot:same-concavity}  
\end{marginfigure}

\begin{warning}
    In addition, concavity can change \textbf{without a point of inflection}.
    This is usually caused by a hole or vertical asymptote that makes the
    point of inflection undefined.
\end{warning}

\begin{warning}
    Like always, do not forget there can be a point of inflection 
    when \(f''(x)\) does not exist! Do \textbf{not} assume that when the derivative
    of a function does not exist at a point, the original function would also
    be undefined at that point. 
\end{warning}

\section{The Mean Value Theorem}{}{}

\begin{theorem}[Mean Value Theorem]\label{thm:mvt}\index{Mean Value Theorem}
    Suppose that $f(x)$ has a derivative on the interval $(a,b)$ and is
    continuous on the interval $[a,b]$.  Then
    \[
    f'(c)=\frac{f(b)-f(a)}{b-a}
    \]
    for some \(a<c<b\).
    (Fowler \& Snapp, 2014)
\end{theorem}

The Mean Value Theorem is illustrated in Fig~\ref{figure:geoMVT}.

\begin{marginfigure}[-1in]
    \begin{adjustbox}{width=\marginparwidth}
    \begin{tikzpicture}
        \begin{axis}[
                xmin=.5, xmax=5.5,ymin=0,ymax=3.1,
                axis lines =center, xlabel=$x$, ylabel=$y$,
                every axis y label/.style={at=(current axis.above origin),anchor=south},
                every axis x label/.style={at=(current axis.right of origin),anchor=west},
                xtick={1,2.04,5}, xticklabels={$a$,$c$,$b$},
                ytickmin=1, ytickmax=0,
                axis on top,
              ] 
              \addplot [draw=none, fill=fill2,domain=(1:5)] {3.1} \closedcycle;       
              \addplot [penColor2!40!background,very thick,dashed] plot coordinates {(1,.84+1.5) (5,1.5-.96)};        
              \addplot [textColor,dashed] plot coordinates {(2.04,0) (2.04,1.5+.89)};        
          \addplot [very thick,penColor, smooth,domain=(1:5)] {sin(deg(x))+1.5};
              \addplot [very thick,penColor2,domain=(.5:5.5)] {-.45*(x-2.04)+.89+1.5};
              %\node at (axis cs:.4,2.5) [penColor] {$f(x)$}; 
              \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(1,.84+1.5)};  %% closed hole          
              \addplot[color=penColor,fill=penColor,only marks,mark=*] coordinates{(5,-.96+1.5)};  %% closed hole          
              \addplot[color=penColor3,fill=penColor3,only marks,mark=*] coordinates{(2.04,.89+1.5)};  %% closed hole          
            \end{axis}
    \end{tikzpicture}
\end{adjustbox}
\caption{A geometric interpretation of the Mean Value Theorem (Fowler \& Snapp, 2014)}\label{figure:geoMVT}
\end{marginfigure}

A special case of the Mean Value Theorem when \(f(a) = f(b)\) is called the \textbf{Rolle's Throrem}.

\begin{theorem}[Rolle's Theorem]\label{thm:rolle}\index{Rolle's Theorem}
    Suppose that $f(x)$ is differentiable on the interval $(a,b)$, is
    continuous on the interval $[a,b]$, and $f(a)=f(b)$. Then 
    \[f'(c)=0\]
    for some \(a<c<b\).(Fowler \& Snapp, 2014)
\end{theorem}



\begin{exercises}
    \noindent Determine whether the following statements are correct.
    \begin{exercise} Relative maximum is the highest point on the graph f(x) in a given interval.
    \begin{answer} No. It's the highest point compared to the neighboring domain.
    \end{answer}\end{exercise}
    
    \begin{exercise} An extremum is either a maximum or a minimum.
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise} An extremum is always a critical point.
    \begin{answer} No. Absoulte extremum can be at endpoints.
    \end{answer}\end{exercise}

    \begin{exercise} The slope at a relative extremum is always 0.
    \begin{answer} No. The point can have no slope at all (not differentiable).
    \end{answer}\end{exercise}

    \begin{exercise} The function can change from increasing to decreasing without a critical point.
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise} If \(f'(c)\) is undefined, then \(f(c)\) must also be undefined.
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise} If \(f(c)\) is undefined, then \(f'(c)\) must also be undefined.
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise} All relative extrema must occur at critical points.
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise} Points of inflection are where the concavity changes.
    \begin{answer} Yes.
    \end{answer}\end{exercise}

    \begin{exercise} Points of inflection are where \(f''(x)=0\) or undefined.
    \begin{answer} No.
    \end{answer}\end{exercise}

    \begin{exercise} The concavity changes when and only when crossing the points of inflections.
    \begin{answer} No.
    \end{answer}\end{exercise}

    \noindent Determine the extrema in the following functions.
    \begin{exercise} \(f(x)=x^2+2x+3\)
    \begin{answer} Relative \& Absolute minimum at \((-1,2)\).
    \end{answer}\end{exercise}
    
    \begin{exercise} \(f(x)=\frac{1}{x}+2x\)
    \begin{answer} Relative maximum at \((-\frac{1}{\sqrt{2}},-2\sqrt{2})\);
        relative minimum at \((\frac{1}{\sqrt{2}},2\sqrt{2})\).
    \end{answer}\end{exercise}
    
    \begin{exercise} \(f(x)=\frac{3x+1}{x^2}\)
    \begin{answer} Global minimum at \((-\frac{2}{3},-\frac{9}{4}))\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\sqrt{x^2}\)
    \begin{answer} Global \& relative minimum at \((0,0))\)
    \end{answer}\end{exercise}
    
    \noindent Determine the inflection points in the following functions.
    \begin{exercise} \(f(x)=x^3+3\)
    \begin{answer} \((0,3)\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{1}{4}x^4-\frac{3}{2}x^2+2x+1\)
    \begin{answer} \((2,-5)\).
    \end{answer}\end{exercise}
    
    \noindent Find all \(c\) if the Rolle's Theorem applies.
    \begin{exercise} \(f(x)=x^3-3x^2+5\) on interval \([0,3]\)
    \begin{answer} \(c=2\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=5x+\frac{3}{x}\) on interval \([\frac{1}{2},1]\)
    \begin{answer} The Rolle's Theorem does not apply because \(f(\frac{1}{2})\neq f(1)\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{x^2}{x+3}\) on interval \([2,6]\)
    \begin{answer} \(c=0\).
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\sqrt[3]{x^2}\) on interval \([-1,1]\)
    \begin{answer} The Rolle's Theorem does not apply because of non-differentiability at x=0.
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{1}{2}\) on interval \([-1,1]\)
    \begin{answer} The Rolle's Theorem does not apply because of discontinuity at x=0.
    \end{answer}\end{exercise}

    \noindent Find all \(c\) if the Mean Value Theorem applies.
    \begin{exercise} \(f(x)=\frac{x}{x+2}\) on interval \([1,4]\) (Fowler \& Snapp, 2014)
    \begin{answer} \(c=\sqrt{18}-2\)
    \end{answer}\end{exercise}

    \begin{exercise} \(f(x)=\frac{3x}{x+7}\) on interval \([-2,6]\) (Fowler \& Snapp, 2014)
    \begin{answer} \(c=\sqrt{65}-7\)
    \end{answer}\end{exercise}
\end{exercises}
  